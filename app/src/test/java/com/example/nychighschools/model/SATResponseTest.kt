package com.example.nychighschools.model

import org.junit.Assert.*
import org.junit.Test

/**
 * Model SATResponse Unit-Test
 */
class SATResponseTest{

    @Test
    fun SATResponse_Model(){
        val obj = SATResponse(
            dbn = "dbn",
            satWritingAvgScore = "satWritingAvgScore",
            satCriticalReadingAvgScore = "satCriticalReadingAvgScore",
            satMathAvgScore = "satMathAvgScore",
            schoolName = "schoolName",
            numOfSatTestTakers = "numOfSatTestTakers"
        )

        assertEquals("dbn", obj.dbn)
        assertEquals("satWritingAvgScore", obj.satWritingAvgScore)
        assertEquals("satCriticalReadingAvgScore", obj.satCriticalReadingAvgScore)
        assertEquals("satMathAvgScore", obj.satMathAvgScore)
        assertEquals("schoolName", obj.schoolName)
        assertEquals("numOfSatTestTakers", obj.numOfSatTestTakers)
    }
}