package com.example.nychighschools.model

import com.google.gson.annotations.SerializedName
import org.junit.Assert.*
import org.junit.Test

/**
 * Model SchoolResponse Unit-Test
 */
class SchoolResponseTest {

    @Test
    fun SchoolResponse_Model() {

        val obj = SchoolResponse(
            dbn = "dbn",
            schoolName = "schoolName",
            primaryAddressLine1 = "primaryAddressLine1",
            city = "city",
            stateCode = "stateCode",
            phoneNumber = "phoneNumber",
            zip = "zip",
            website = "website"
        )

        assertEquals("dbn", obj.dbn)
        assertEquals("schoolName", obj.schoolName)
        assertEquals("primaryAddressLine1", obj.primaryAddressLine1)
        assertEquals("city", obj.city)
        assertEquals("zip", obj.zip)
        assertEquals("stateCode", obj.stateCode)
        assertEquals("phoneNumber", obj.phoneNumber)
        assertEquals("website", obj.website)

    }
}