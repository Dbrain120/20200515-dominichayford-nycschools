package com.example.nychighschools.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nychighschools.testUtils.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.Matchers.not
import org.hamcrest.Matchers.nullValue
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
/**
 * ViewModel Unit-Testing
 */
@RunWith(AndroidJUnit4::class)
class SchoolViewModelTest{
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: SchoolViewModel

    /**
     * Set the main Coroutines dispatcher for unit testing.
      */
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setUpViewModel(){
        viewModel = SchoolViewModel(ApplicationProvider.getApplicationContext())
    }

    /**
     * Testing the fetchSchools data function
     */
    @Test
    fun fetchSchools_Calls(){
        viewModel.fetchSchools()
        assertThat(viewModel.schoolObservable(), not(nullValue()))
    }

    /**
     * Testing the fetchSATResults data function
     */
    @Test
    fun fetchSATResults_Calls(){
        viewModel.fetchSATResults()
        assertThat(viewModel.satObservable(), not(nullValue()))
    }


}

