package com.example.nychighschools.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nychighschools.databinding.SchoolListBinding
import com.example.nychighschools.model.SchoolResponse

/**
 * this is an adapter for implementation
 */

class SchoolAdapter(val listener : (Int) -> Unit) : RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>(){

    private var dataList : List<SchoolResponse> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(SchoolListBinding.inflate(LayoutInflater.from(parent.context)))
    }
    override fun getItemCount(): Int {
        return dataList.size
    }
    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val school = dataList.get(position)
        holder.bind.mschoolResponse = school
        holder.bind.cvSchools.setOnClickListener{listener(position)}
    }
    fun loadData(data: List<SchoolResponse>){
        Log.d("TAG",data[0].toString())
        dataList = data
        notifyDataSetChanged()
    }

    class SchoolViewHolder(binding: SchoolListBinding) : RecyclerView.ViewHolder(binding.root)
    {val bind = binding}
}