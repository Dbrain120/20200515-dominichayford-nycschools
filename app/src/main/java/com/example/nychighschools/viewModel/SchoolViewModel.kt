package com.example.nychighschools.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.example.nychighschools.model.SATResponse
import com.example.nychighschools.model.SchoolResponse
import com.example.nychighschools.remote.SchoolRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SchoolViewModel (application: Application) : AndroidViewModel(application){
    private val schools = MutableLiveData<List<SchoolResponse>>()
    private val satResponse = MutableLiveData<List<SATResponse>>()
    private val repo = SchoolRepository

    fun schoolObservable(): LiveData<List<SchoolResponse>> = schools
    fun satObservable(): LiveData<List<SATResponse>> = satResponse

    fun fetchSchools(){
        viewModelScope.launch {
            schools.value = repo.getSchools()
        }
    }
    fun fetchSATResults(){
        viewModelScope.launch {
            satResponse.value = repo.getSatResults()
        }
    }

}