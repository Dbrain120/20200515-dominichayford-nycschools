package com.example.nychighschools.remote

import com.example.nychighschools.model.SATResponse
import com.example.nychighschools.model.SchoolResponse
/**
 * This is the Repository class that handles network calls with the Service Instance
 * Note: Implementing Coroutines
 * */
object SchoolRepository {

    private val apiService =
        SchoolInstance.getInstance
        .create(SchoolService::class.java)

    suspend fun getSchools() : List<SchoolResponse> = apiService.getSchools()
    suspend fun getSatResults(): List<SATResponse> = apiService.getSatResults()


}