package com.example.nychighschools.remote

import com.example.nychighschools.model.SATResponse
import com.example.nychighschools.model.SchoolResponse
import retrofit2.http.GET

/**
 * this interface specify which path to get the data
 */
interface SchoolService {
    @GET(SCHOOL_PATH)
    suspend fun getSchools() : List<SchoolResponse>

    @GET(SAT_RESULT_PATH)
    suspend fun getSatResults() : List<SATResponse>

    companion object{
        const val SCHOOL_PATH = "resource/s3k6-pzi2.json"
        const val SAT_RESULT_PATH = "resource/f9bf-2cp4.json"
    }

}
