package com.example.nychighschools.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.nychighschools.databinding.FragmentDetailBinding
import com.example.nychighschools.model.SATResponse

/**
 * A simple Detail Fragment subclass.
 */
class DetailFragment : Fragment() {
    private var _bind : FragmentDetailBinding? = null
    private val bind get() = _bind

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _bind = FragmentDetailBinding.inflate(inflater, container, false)
        /**
         * below get StringArrays from home fragment and set it to the positions
         */
        val satResults = arguments?.getStringArray("satResult")
        val satResult = SATResponse(
            satResults?.get(0),
            satResults?.get(1),
            satResults?.get(2),
            satResults?.get(3),
            satResults?.get(4),
            satResults?.get(5))

        bind?.satResponse = satResult

        return bind?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _bind = null
    }

}
