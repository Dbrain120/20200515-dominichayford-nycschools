package com.example.nychighschools.view

import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nychighschools.databinding.FragmentHomeBinding

import com.example.nychighschools.R
import com.example.nychighschools.adapter.SchoolAdapter
import com.example.nychighschools.model.SATResponse
import com.example.nychighschools.model.SchoolResponse
import com.example.nychighschools.viewModel.SchoolViewModel
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    /**
     *  below is the variables
     */
    private var _bind: FragmentHomeBinding? = null
    private val bind get() = _bind
    private var schools : List<SchoolResponse> = ArrayList()
    private var satResult : List<SATResponse> = ArrayList()
    private lateinit var mAdapter : SchoolAdapter
    private lateinit var viewModel : SchoolViewModel
    private var dbn: String? = null

    /**
     * This is where the adapter is set to position and inflate the binding
     * */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lazyLoadData()
        _bind = FragmentHomeBinding.inflate(inflater, container,false)
        mAdapter = SchoolAdapter { position -> dbn = schools[position].dbn
            launchNavigation()
        }
        return bind?.root
    }

    override fun onStart() {
        super.onStart()
        rv_schools.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = mAdapter
        }

    }

    /**
     * lazyLoadData() Method it observe the data and notify data change
     * */
    private fun lazyLoadData() {
        viewModel = ViewModelProvider(this).get(SchoolViewModel::class.java)
        viewModel.schoolObservable().observe(viewLifecycleOwner, Observer { schoolsList ->
            schools = schoolsList
            mAdapter.loadData(schools)
            mAdapter.notifyDataSetChanged()
        })
        viewModel.fetchSchools()
        viewModel.satObservable().observe(viewLifecycleOwner, Observer { satResultList ->
            satResult = satResultList  })
        viewModel.fetchSATResults()
    }

    /**
     * launchNavigation() method gets data SATResponse and
     * navigate from home fragment and send it to the detail fragment
     * */
    private fun launchNavigation() {
        val satResponse = getSATResult()
        val satResult = arrayOf(satResponse.dbn.toString(),
                        satResponse.satWritingAvgScore.toString(),
                        satResponse.satCriticalReadingAvgScore.toString(),
                        satResponse.satMathAvgScore.toString(),
                        satResponse.schoolName.toString(),
                        satResponse.numOfSatTestTakers.toString())
        val bundle = bundleOf("satResult" to satResult)
        this.findNavController().navigate(R.id.action_homeFragment_to_detailFragment, bundle)
    }

    private fun getSATResult(): SATResponse {
        var model = SATResponse()
        satResult.forEach { school ->
            if (dbn == school.dbn)
                model = school
        }

        return model
    }

    override fun onDestroy() {
        super.onDestroy()
        _bind = null
    }

}
